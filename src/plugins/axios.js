export default function ({ $axios }, inject) {

    // Create a custom axios instance
    const NODE_API = $axios.create();
  
    // Set baseURL
    NODE_API.setBaseURL(process.env.BASE_URL);
  
 
  
    // Set headers

    NODE_API.setHeader('Content-Type', 'application/json');
  
    // Inject to context as $PS_API
    inject('NODE_API', NODE_API);
  
  }
  