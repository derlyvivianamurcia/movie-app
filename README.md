## Project Title
Movie App

## Description 📋

MovieApp (Api themoviedb) es una aplicación donde se puede consultar las diferentes peliculas y algunas carácteristicas de estas.

## Cloning a repository using the command line 💻

1. En GitHub, visita la página principal del repositorio.

2. Debajo del nombre del repositorio, haga clic en Clonar o descargar .

3. Para clonar el repositorio utilizando HTTPS, en "Clonar con HTTPS", haga clic en . Para clonar el repositorio utilizando una clave SSH, incluido un certificado emitido por la autoridad de certificación SSH de su organización, haga clic en Usar SSH y luego haga clic en 📋.

4. Abre Git Bash .

5. Cambia el directorio de trabajo actual a la ubicación donde deseas que se clone el directorio.

6. Escribe git clone, y luego pega la URL que copiaste en el Paso 2.

$ git clone 

7. Presiona Enter (Intro). Se creará tu clon local.

## Installation 🔧

Para comenzar a utilizar React debemos instalar algunas paquetes:

Dependencias
1. npm install 
2. ### `npm run serve` 

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
